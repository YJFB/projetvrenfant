# À la place d'un bébé

Ce projet est une vidéo VR non-interactive pour se mettre à la place d'un bébé dans un crèche.
Il a été développé en collaboration avec une crèche bordelaise pour une journée de formation en 2019.

## Installation

Ce projet est utilisable sur des systèmes Android récents. Il suffit de se munir du fichier ./À la place d'un bébé.apk
