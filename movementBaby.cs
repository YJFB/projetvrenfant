﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movementBaby : MonoBehaviour
{
    static float time;
    static float dep;
    //private Rigidbody rb;
    // Start is called before the first frame update
    static float DISTANCE;
    private Vector3 velocity = Vector3.zero;
    void Start()
    {
        time = 0;
        DISTANCE = .47f;
        //StartCoroutine(sequenceMovements());
        dep = DISTANCE * Time.fixedDeltaTime;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        time = time + Time.fixedDeltaTime;
        path();



    }

    void path()
    {
        /*
         moveTo(new Vector3(0f, 0f, -DISTANCE * Time.deltaTime), 6, 10);
         moveTo(new Vector3(-DISTANCE * Time.deltaTime, 0f, 0f), 14, 18);
         moveTo(new Vector3(0f, DISTANCE/8 * Time.deltaTime, 0f), 20, 23);
         moveTo(new Vector3(0f, -DISTANCE/8 * Time.deltaTime, 0f), 30, 33);
         moveTo(new Vector3(-DISTANCE * Time.deltaTime, 0f, 0f), 37, 43);
         moveTo(new Vector3(DISTANCE * Time.deltaTime, 0f, 0f), 46, 50);
         moveTo(new Vector3(-DISTANCE * Time.deltaTime, 0f, 0f), 53, 54);
         */

        float beg;
        beg = 60;
        float deg;
        deg = beg + 180;
        GameObject cam;
        cam = this.gameObject.transform.GetChild(0).gameObject;

        // Premier mouvement vers femme 
        moveTo(new Vector3(-1f, 0f, .5f).normalized * dep, beg + 0, 2);
        // Deuxième mouvement vers femme 
        moveTo(new Vector3(-1f, 0f, .8f).normalized * dep, beg + 3, 4.7f);
        // Montée devant femme 
        moveTo(new Vector3(0f, .5f, 0f).normalized * dep, beg + 11, 2.5f);
        // Descente devant femme 
        moveTo(new Vector3(0f, -.5f, 0f).normalized * dep, beg + 25, 2.5f);
        // Mouvement vers fond 
        moveTo(new Vector3(-1f, 0f, 0f).normalized * dep, beg + 29, 4f);

       // Rotation 
        rotate(new Vector3(0f, 180f, 0f), beg + 42.5f, 4, cam);

        // Retour du fond 
        moveTo(new Vector3(1f, 0f, 0f).normalized * dep, beg + 47, 4f);
        // Montée 
        moveTo(new Vector3(0f, .5f, 0f).normalized * dep, beg + 52, 2.5f);
        // Descente 
        moveTo(new Vector3(0f, -.5f, 0f).normalized * dep, beg + 64.5f, 2.5f);
        // Retour premier mouvement 
        moveTo(new Vector3(1f, 0f, -.8f).normalized * dep, beg + 68, 2);
        // Retour deuxième mouvement 
        moveTo(new Vector3(1f, 0f, -.5f).normalized * dep, beg + 70, 4.7f);





        // Premier mouvement vers femme 
        moveTo(new Vector3(-1f, 0f, .5f).normalized * dep, deg + 0, 2);
        // Deuxième mouvement vers femme 
        moveTo(new Vector3(-1f, 0f, .8f).normalized * dep, deg + 3, 4.7f);
        // Montée devant femme 
        moveTo(new Vector3(0f, .5f, 0f).normalized * dep, deg + 11, 2.5f);
        // Descente devant femme 
        moveTo(new Vector3(0f, -.5f, 0f).normalized * dep, deg + 25, 2.5f);
        // Mouvement vers fond 
        moveTo(new Vector3(-1f, 0f, 0f).normalized * dep, deg + 29, 4f);

        // Rotation 
        rotate(new Vector3(0f, 180f, 0f), 43, 4, cam);

        // Retour du fond 
        moveTo(new Vector3(1f, 0f, 0f).normalized * dep, deg + 47, 4f);
        // Montée 
        moveTo(new Vector3(0f, .5f, 0f).normalized * dep, deg + 52, 2.5f);
        // Descente 
        moveTo(new Vector3(0f, -.5f, 0f).normalized * dep, deg + 64.5f, 2.5f);
        // Retour premier mouvement 
        moveTo(new Vector3(1f, 0f, -.8f).normalized * dep, deg + 68, 2);
        // Retour deuxième mouvement 
        moveTo(new Vector3(1f, 0f, -.5f).normalized * dep, deg + 70, 4.5f);

    }
    void moveTo(Vector3 target, float tStart, float duration)
    {
        if(tStart < time && time < tStart + duration)
        {
            transform.Translate(translation: target);
        }
    }
    void rotate(Vector3 target, float tStart, float duration, GameObject cam)
    {
        if (tStart < time && time < tStart + duration)
        {
            cam.transform.Rotate(Time.fixedDeltaTime * target/duration/2);
        }
    }



    IEnumerator sequenceMovements()
    {
        //moveTo(new Vector3(0, 0, -.003f), 3);
        yield return new WaitForSeconds(2);
        //transform.Translate(new Vector3(-DISTANCE, 0f, 0f) * Time.deltaTime);
        yield return new WaitForSeconds(2);
        //transform.Translate(new Vector3(-DISTANCE, 0f, 0f) * Time.deltaTime);
    }


}
